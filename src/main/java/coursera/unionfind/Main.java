package coursera.unionfind;

import java.io.InputStream;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Exercise 1");
        int numberOfMembers = Integer.parseInt(args[0]);
        String filename = args[1];
        exercise1(numberOfMembers, filename);
        System.out.println("Exercise 2");
        exercise2();
        System.out.println("Exercise 3");
        exercise3();
    }

    public static void exercise1(int numberOfMembers, String filename) {
        UnionFind unionFind = new UnionFind(numberOfMembers);
        InputStream is = unionFind.getClass().getResourceAsStream(filename);
        System.setIn(is);
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            long unixTimestamp = scanner.nextLong();
            if (--numberOfMembers <= 1) {
                System.out.println(unixTimestamp);
                break;
            }
            unionFind.union(numberOfMembers, numberOfMembers-1);
        }
    }

    public static void exercise2() {
        int numberOfMembers = 10;
        UnionFind unionFind = new UnionFind(numberOfMembers);
        unionFind.union(7, 2);
        unionFind.union(0, 2);
        unionFind.union(4, 9);
        unionFind.union(5, 1);
        unionFind.union(5, 4);

        System.out.println("find(0) {0, 2, 7} = " + unionFind.find(0));
        System.out.println("find(1) {1, 4, 5, 9} = " + unionFind.find(4));
        System.out.println("find(3) {3} = " + unionFind.find(3));
    }

    public static void exercise3() {
        int numberOfMembers = 10;

        System.out.println("S={0,1,2,...,9}");
        UnionFind unionFind = new UnionFind(numberOfMembers);

        System.out.println("remove 3");
        unionFind.remove(3);
        System.out.println("successor(3) = " + unionFind.successor(3));

        System.out.println("remove 5");
        unionFind.remove(5);
        System.out.println("successor(5) = " + unionFind.successor(5));
        System.out.println("successor(4) = " + unionFind.successor(4));

        System.out.println("remove 4");
        unionFind.remove(4);
        System.out.println("successor(4) = " + unionFind.successor(4));

        System.out.println("remove 8");
        unionFind.remove(8);
        System.out.println("successor(8) = " + unionFind.successor(8));
    }

}
