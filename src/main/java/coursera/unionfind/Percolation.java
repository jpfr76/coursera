package coursera.unionfind;

import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {
    private final int n;
    private final Node[][] grid;
    private final WeightedQuickUnionUF uf;
    private final int virtualTopId;
    private final int virtualBottomId;
    private int numberOfOpenSites;

    public Percolation(int n) {
        validateInputArgs(n);
        this.n = n;
        this.uf = new WeightedQuickUnionUF(n*n+2);
        this.virtualTopId = n*n;
        this.virtualBottomId = n*n+1;
        this.grid = new Node[n][];
        this.numberOfOpenSites = 0;
        for (int i = 0; i < n; i++) {
            this.grid[i] = new Node[n];
            for (int j = 0; j < n; j++) {
                Node node = new Node(i*n+j);
                this.grid[i][j] = node;
                if (i == 0) {
                    this.uf.union(virtualTopId, node.getId());
                }
                if (i == n-1) {
                    this.uf.union(virtualBottomId, node.getId());
                }
            }
        }
    }

    private void validateInputArgs(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException();
        }
    }

    private void validateInputArgs(int row, int col) {
        if (row <= 0 || row > n || col <= 0 || col > n) {
            throw new IllegalArgumentException();
        }
    }

    public void open(int row, int col) {
        validateInputArgs(row, col);
        row--;
        col--;
        Node node = this.grid[row][col];
        if (node.isOpen()) {
            return;
        }
        node.setOpen(true);
        incNumberOfOpenSites();

        boolean hasLeftNeighbour = col > 0;
        boolean hasRightNeighbour = col < (n-1);
        boolean hasTopNeighbour = row > 0;
        boolean hasBottomNeighbour = row < (n-1);

        if (hasLeftNeighbour) {
            Node leftNode = this.grid[row][col-1];
            if (leftNode.isOpen()) {
                this.uf.union(node.getId(), leftNode.getId());
            }
        }
        if (hasRightNeighbour) {
            Node rightNode = this.grid[row][col+1];
            if (rightNode.isOpen()) {
                this.uf.union(node.getId(), rightNode.getId());
            }
        }
        if (hasTopNeighbour) {
            Node topNode = this.grid[row-1][col];
            if (topNode.isOpen()) {
                this.uf.union(node.getId(), topNode.getId());
            }
        }
        if (hasBottomNeighbour) {
            Node bottomNode = this.grid[row+1][col];
            if (bottomNode.isOpen()) {
                this.uf.union(node.getId(), bottomNode.getId());
            }
        }
    }

    private void incNumberOfOpenSites() {
        this.numberOfOpenSites++;
    }

    public boolean isOpen(int row, int col) {
        validateInputArgs(row, col);
        row--;
        col--;
        return this.grid[row][col].isOpen();
    }

    public boolean isFull(int row, int col) {
        validateInputArgs(row, col);
        row--;
        col--;
        Node node = this.grid[row][col];
        return node.isOpen()
                && this.uf.connected(virtualTopId, node.getId());
    }

    public int numberOfOpenSites() {
        return this.numberOfOpenSites;
    }

    public boolean percolates() {
        if (n == 1 && !this.grid[0][0].isOpen()) { return false; }
        return this.uf.connected(virtualTopId, virtualBottomId);
    }
}