package coursera.unionfind;

import java.util.Arrays;

public class UnionFind {
    private int[] sizeTrees;
    private int[] maxMemberInTree;
    private int[] members;

    public UnionFind(int numberOfMembers) {
        this.sizeTrees = new int[numberOfMembers];
        this.members = new int[numberOfMembers];
        this.maxMemberInTree = new int[numberOfMembers];
        Arrays.fill(sizeTrees, 1);
        Arrays.setAll(members, i -> i);
        Arrays.setAll(maxMemberInTree, i -> i);
    }

    private int root(int member) {
        int max = 0;
        while (member != members[member]) {
            member = members[member];
            if (member > max) max = member;
        }
        if (max > maxMemberInTree[member]) {
            maxMemberInTree[member] = member;
        }
        return member;
    }

    public boolean connected(int member1, int member2) {
        return root(member1) == root(member2);
    }

    public void union(int member1, int member2) {
        int root1 = root(member1);
        int root2 = root(member2);
        int maxMemberRoot1 = maxMemberInTree[root1];
        int maxMemberRoot2 = maxMemberInTree[root2];
        if (root1 == root2) return;
        if (sizeTrees[root1] > sizeTrees[root2]) {
            members[member2] = root1;
            sizeTrees[root1] += sizeTrees[root2];
            if (maxMemberRoot2 > maxMemberRoot1) {
                maxMemberInTree[root1] = maxMemberRoot2;
            }
        } else {
            members[member1] = root2;
            sizeTrees[root2] += sizeTrees[root1];
            if (maxMemberRoot1 > maxMemberRoot2) {
                maxMemberInTree[root2] = maxMemberRoot1;
            }
        }
    }

    public int find(int member) {
        int root = root(member);
        return maxMemberInTree[root];
    }

    public void remove(int member) {
        union(member, member+1);
    }

    public int successor(int member) {
        int root = root(member);
        return maxMemberInTree[root];
    }
}
