package coursera.unionfind;

import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
    private final int trials;
    private final double mean;
    private final double stddev;

    public PercolationStats(int n, int trials) {
        validateInputArgs(n, trials);
        this.trials = trials;
        double[] xt = new double[trials];
        for (int trial = 0; trial < trials; trial++) {
            Percolation percolation = new Percolation(n);
            while (!percolation.percolates()) {
                int row = StdRandom.uniform(1, n+1);
                int col = StdRandom.uniform(1, n+1);
                percolation.open(row, col);
            }
            double numberOfOpenSites = percolation.numberOfOpenSites();
            xt[trial] = numberOfOpenSites/(n*n);
        }
        this.mean = StdStats.mean(xt);
        this.stddev = StdStats.stddev(xt);
    }

    private void validateInputArgs(int n, int trials) {
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException();
        }
    }

    public double mean() {
        return mean;
    }

    public double stddev() {
        return stddev;
    }

    public double confidenceLo() {
        return mean - 1.96*stddev/Math.sqrt(trials);
    }

    public double confidenceHi() {
        return mean + 1.96*stddev/Math.sqrt(trials);
    }

    public String toString() {
        return "mean = " + mean + "\n" +
                "stddev = " + stddev + "\n" +
                "95% confidence interval = [" + confidenceLo() + ", " + confidenceHi() + "]\n";
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        int trials = Integer.parseInt(args[1]);
        PercolationStats stats = new PercolationStats(n, trials);
        System.out.println(stats.toString());
    }
}